const FS = require('fs');
const GULP = require('gulp');
const RUN = require('gulp-run');
const JSDOC = require('gulp-jsdoc3');
const MOCHA = require('gulp-mocha');
const DEL = require('del');
const TESTS = [
	'./lambdas/*/test.js',
];

// Generate Documentation
GULP.task('Generate Documentation', function (cb) {
	var config = {
		"tags": {
			"allowUnknownTags": true
		},
		"opts": {
			"destination": "./documentation",
			"tutorials": "./documentation"
		},
		"plugins": [
			"plugins/markdown"
		],
		"source": {
			"include": ["./readme.md"],
			"includePattern": ".+\\.js(doc)?$",
			"excludePattern": "(^|\\/|\\\\)_"
		},
		"outputSourceFiles": true,
		"templates": {
			"cleverLinks": false,
			"monospaceLinks": false,
			"default": {
				"outputSourceFiles": true
			},
			"path": "ink-docstrap",
			"theme": "cerulean",
			"navType": "vertical",
			"linenums": true,
			"dateFormat": "MMMM Do YYYY, h:mm:ss a"
		}
	};
	var src = [
		'./lambdas/*/index.js'
	];

	try{
		DEL(config.opts.destination).then((r)=>{
			FS.mkdirSync(config.opts.destination);
			GULP.src(src, {read: false}).pipe(JSDOC(config, cb));
		});
	} catch(e) {
		FS.mkdirSync(config.opts.destination);
		GULP.src(src, {read: false}).pipe(JSDOC(config, cb));
	}

	return GULP;

});
// Default gulp task
GULP.task('default', ['watch']);
// Run all tests
GULP.task('Run Tests', function() {
	process.env.NODE_ENV = 'test';
	return GULP.src(TESTS, { read: false })
		.pipe(MOCHA({
			reporter: 'spec'
		}));
});
// Watcher
GULP.task('watch', function() {
	// Watch and test individual functions
	GULP.watch(['./lambdas/*/*.js'], ['Run Tests', 'Generate Documentation']);
});
