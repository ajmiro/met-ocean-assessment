"use strict";
/**
 * This lambda function fetches the data base given a date INVALID_DATE_RANGE
 * @module Lambdas/getData
 * @requires fs
 * @requires moment-timezone
 */
const CONSTANTS = require('./constants.js')
const FS = require('fs')
const path = require('path');
const MOMENT = require('moment')

// MOMENT.tz.setDefault("UTC")

let handlerCallback = null

/**
 * Process complete. Wraps the response with correct headers and statusCodes for API Gateway.
 * @param {Object} error
 * @param {Object} result
 * @param {Function} callback
 * @return {void}
 */
function done(error, result) {
	if(error)
	{
		handlerCallback(null, {
			statusCode: 500,
			body: JSON.stringify(error)
		})
	}
	else
	{
		handlerCallback(null, {
			statusCode: 200,
			body: JSON.stringify(result)
		})
	}
}
/**
 * Check data entered is valid or not
 * @param  {Object} dateRange dateEnd and dateStart in ISO format
 * @return {Object} {valid: Boolean, error: String (optional)}
 */
function checkValidDates( dateRange )
{
	let result = { valid: false }

	if( dateRange.dateStart && dateRange.dateEnd ) {
		try{
			let dateStart = new MOMENT(dateRange.dateStart),
				dateEnd = new MOMENT(dateRange.dateEnd)

			if( dateStart.isBefore(dateEnd))
			{
				result.valid = true
			}
			else
			{
				result.error = CONSTANTS.INVALID_DATE_RANGE
			}
		} catch(error) {
			result.error = CONSTANTS.INVALID_DATE
		}
	}
	else {
		result.error = CONSTANTS.DATE_RANGE_MISSING
	}

	return result
}

/**
 * Gets data from data.json file or S3 bucket in production
 * @return {array} list of data
 */
function loadData(){
    return new Promise((resolve, reject)  => {
			try {
				let a = FS.readFileSync(path.resolve(__dirname, 'data.json'), 'utf-8')
				resolve(JSON.parse(a))
			} catch(e) {
				reject(e)
			}
    })
}

/**
 * Main handler function
 * @param  {object}   event
 * @param  {object}   context
 * @param  {Function} callback
 * @return {void}
 */
exports.handler = (event, context, callback) => {
	try{

        handlerCallback = callback

		if(event.queryStringParameters === null)
		{
			return done({ valid: false, error: CONSTANTS.INVALID_DATE })
		}
		else
		{
			let isValidDates = checkValidDates(event.queryStringParameters),
				variableName = event.queryStringParameters.name,
				method = (event.queryStringParameters.method === undefined) ? CONSTANTS.NOCOMPUTATION : event.queryStringParameters.method

				if( isValidDates.valid && variableName !== null )
				{
					return loadData()
							.then( result => {
								let dateStart = new MOMENT(event.queryStringParameters.dateStart),
									dateEnd = new MOMENT(event.queryStringParameters.dateEnd),
									temp = [],
									finalData = []

								if( result.length > 0 ) {

									temp = result.filter( item => {
											let a = new MOMENT(item['datetime'])
											if((item[variableName] !== "null") && a.isSameOrAfter(dateStart) && a.isSameOrBefore(dateEnd)) {
												return item
											}
										}).map(function(item) {
											return parseFloat(item[variableName]);
										})

									if( method !== CONSTANTS.NOCOMPUTATION )
									{
										finalData = (temp.length == 0 ) ? 0 : temp.reduce((accumulator, currentValue) => accumulator + currentValue) / temp.length

									}
									else
									{
										finalData = temp
									}

									return done(null, finalData )

								}
								else
								{
									return done(null, [])
								}

							})
							.catch( error => {
								return done(error)
							})

				}
				else
				{
					return done( {isvalid: false, error: CONSTANTS.MISSING_QUERY_PARAMETERS})
				}
		}

	} catch (e) {
		done(e)
	}

}
