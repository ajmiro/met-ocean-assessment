module.exports = {
	INVALID_DATE: 'Date input is invalid',
	INVALID_DATE_RANGE: 'Date range is invalid',
	NO_DATA: 'Data does not exists',
	MISSING_QUERY_PARAMETERS: 'Missing query parameters',
	MEAN: 'mean',
	NOCOMPUTATION: 'nocomputation'
}
