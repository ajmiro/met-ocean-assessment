'use strict'
// Constants
const EXPECT = require('chai').expect
const LAMBDA = require('./index.js')
const FS = require('fs')
const PATH = require('path')
const CONSTANTS = Object.assign(require('./constants.js'))
const CONTEXT = {functionName: PATH.basename(__dirname)}

process.env.NODE_ENV = 'test'

// The tests
describe("Lambda: " + CONTEXT.functionName, function() {
	let event = {
	    "resource": "/get-data",
	    "path": "/get-data",
	    "httpMethod": "GET",
	    "headers": {
	        "Content-Type": "application/json",
	        "Host": "muag43srvh.execute-api.us-east-1.amazonaws.com",
	        "X-Accelo-Event": "get_data",
	        "X-Amzn-Trace-Id": "Root=1-5b58db4b-298eeaf856eb2b4a6824ece6",
	        "X-Forwarded-For": "54.200.89.239",
	        "X-Forwarded-Port": "443",
	        "X-Forwarded-Proto": "https"
	    },
	    "queryStringParameters": null,
	    "pathParameters": null,
	    "stageVariables": null,
	    "requestContext": {
	        "resourceId": "zgruqp",
	        "resourcePath": "/get-data",
	        "httpMethod": "GET",
	        "extendedRequestId": "KmczuFCWIAMFc6g=",
	        "requestTime": "25/Jul/2018:20:19:23 +0000",
	        "path": "/dev/get-data",
	        "accountId": "605367074299",
	        "protocol": "HTTP/1.1",
	        "stage": "dev",
	        "requestTimeEpoch": 1532549963087,
	        "requestId": "05021e98-9048-11e8-8db2-e3a5f7f3db43",
	        "identity": {
	            "cognitoIdentityPoolId": null,
	            "accountId": null,
	            "cognitoIdentityId": null,
	            "caller": null,
	            "sourceIp": "54.200.89.239",
	            "accessKey": null,
	            "cognitoAuthenticationType": null,
	            "cognitoAuthenticationProvider": null,
	            "userArn": null,
	            "userAgent": null,
	            "user": null
	        },
	        "apiId": "muag43srvh"
	    },
	    "isBase64Encoded": false
	}

	it('Should return error if date is invalid or missing', function(done){
		this.timeout(5000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				let body = JSON.parse(result.body)
				EXPECT(result.statusCode).to.equal(500)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should return error if date range is invalid', function(done){
		this.timeout(5000);
		event.queryStringParameters = {
		        dateStart:'2019-01-30',
		        dateEnd:'2019-01-01'
		    }
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				let body = JSON.parse(result.body)
				EXPECT(result.statusCode).to.equal(500)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should return error variableName does not exists', function(done){
		event.queryStringParameters = {
		        dateEnd:'2018-11-01',
		        dateStart:'2018-11-10',
				name: 'test'
		    }
		this.timeout(5000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				let body = JSON.parse(result.body)
				EXPECT(body.isvalid).to.equal(false)
				EXPECT(result.statusCode).to.equal(500)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should return empty array if date range does not exists in data', function(done){
		event.queryStringParameters = {
		        dateEnd:'2019-01-30',
		        dateStart:'2019-01-01',
				name: 'sea_surface_wave_significant_height'
		    }
		this.timeout(5000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				let body = JSON.parse(result.body)
				EXPECT(body.length).to.equal(0)
				EXPECT(result.statusCode).to.equal(200)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should return data within the date range given', function(done){
		event.queryStringParameters = {
		        dateEnd:'2018-11-12',
		        dateStart:'2018-11-01',
				name: 'sea_surface_wave_significant_height'
		    }
		this.timeout(10000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				let body = JSON.parse(result.body)
				EXPECT(body.length).not.to.equal(0)
				EXPECT(result.statusCode).to.equal(200)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
	it('Should return mean average within the date range given', function(done){
		event.queryStringParameters = {
		        dateEnd:'2018-11-12',
		        dateStart:'2018-11-01',
				name: 'sea_surface_wave_significant_height',
				method: CONSTANTS.MEAN
		    }
		this.timeout(10000);
		LAMBDA.handler(event, {}, (error, result) => {
			try{
				let body = JSON.parse(result.body)
				EXPECT(body.length).not.to.equal(0)
				EXPECT(result.statusCode).to.equal(200)
				done()
			} catch(e) {
				done(e)
			}
		})
	})
})
